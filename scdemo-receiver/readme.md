本模块用于演示rabbitMQ消息接收

#### 步骤：
1. 导入需要的依赖spring-boot-starter-amqp
2. 创建项目属性文件application.properties
3. 在项目配置文件中配置RabbitMQ
4. 编写启动类
5. 编写消息接收处理器