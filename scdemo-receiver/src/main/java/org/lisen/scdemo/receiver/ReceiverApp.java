package org.lisen.scdemo.receiver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Administrator
 * @create 2020-02-1523:58
 */
@SpringBootApplication
public class ReceiverApp {

    public static void main(String[] args) {
        SpringApplication.run(ReceiverApp.class, args);
    }

}
