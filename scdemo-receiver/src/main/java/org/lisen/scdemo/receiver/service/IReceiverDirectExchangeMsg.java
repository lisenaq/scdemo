package org.lisen.scdemo.receiver.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author Administrator
 * @create 2020-02-2021:28
 */
public interface IReceiverDirectExchangeMsg {

    void receive(String msg);

}
