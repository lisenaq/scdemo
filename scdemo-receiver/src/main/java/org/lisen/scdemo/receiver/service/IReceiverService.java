package org.lisen.scdemo.receiver.service;

/**
 * @author Administrator
 * @create 2020-02-1600:03
 */
public interface IReceiverService {

    void receive(String msg);

    /**
     * @author Administrator
     * @create 2020-02-2021:32
     */
    class ReceiverDirectExchangeMsgImpl implements IReceiverDirectExchangeMsg {
        @Override
        public void receive(String msg) {

        }
    }
}
