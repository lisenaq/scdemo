package org.lisen.scdemo.receiver.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author Administrator
 * @create 2020-02-2021:33
 */
@Component
@Slf4j
//queues参数指定的是与直接交换机关联的队列名称
//@RabbitListener(queues = "direct.queue")
public class ReceiverDirectExchangeMsgImpl implements IReceiverDirectExchangeMsg {

    @Override
    //@RabbitHandler
    public void receive(String msg) {
        log.info("接收通过直接交换机发送的消息： " + msg);
    }

}
