package org.lisen.scdemo.receiver.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author Administrator
 * @create 2020-02-1600:04
 */
@Component
@Slf4j
@RabbitListener(queues="hello")
public class ReceiverServiceImpl implements IReceiverService {

    @Override
    @RabbitHandler
    public void receive(String msg) {
        log.info("Receiver : {}", msg);
    }

}
