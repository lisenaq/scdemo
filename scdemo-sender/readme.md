本模块用于演示rabbitMQ消息发送

#### 步骤：
1. 引入依赖包
2. 编写项目配置文件application.properties
3. 在属性文件中配置RabbitMQ
4. 创建启动类
5. 创建配置类，本示例中为config包中的RabbitMQConfig
6. 创建Service，并注入RabbitTemplate用于发送消息
7. 创建测试类,发送消息

安装配置好RabbitMQ运行测试用例