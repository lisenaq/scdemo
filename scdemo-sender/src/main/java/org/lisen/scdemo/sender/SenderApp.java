package org.lisen.scdemo.sender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Administrator
 * @create 2020-02-1518:53
 */
@SpringBootApplication
public class SenderApp {

    public static void main(String[] args) {
        SpringApplication.run(SenderApp.class, args);
    }

}
