package org.lisen.scdemo.sender.controller;

import org.lisen.scdemo.sender.service.ISenderService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Administrator
 * @create 2020-02-2820:44
 */
@RestController
public class SendMsgController {

    @Resource
    private ISenderService senderService;

    @RequestMapping("/send")
    public Object send() {
        senderService.confirmMessage();
        Map<String,Object> map = new HashMap<>();
        map.put("code", 1);
        map.put("msg", "发送成功");
        return map;
    }

}
