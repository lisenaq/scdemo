package org.lisen.scdemo.sender.service;

/**
 * @author Administrator
 * @create 2020-02-1521:46
 */
public interface ISenderService {

    void send();

    void directExchangeSend();

    void topicExchangeSend();

    void fanoutExchangeSend();

    void dxlExchangeSend();

    void delayQueue();

    void confirmMessage();
}
