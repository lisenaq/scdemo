package org.lisen.scdemo.sender;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.lisen.scdemo.sender.service.ISenderService;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * @author Administrator
 * @create 2020-02-1521:15
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SenderApp.class)
public class SendersMsgTest {

    @Resource
    private ISenderService senderService;


    @Test
    public void testSend() {
        senderService.send();
    }


    //使用Direct型交换机发送消息
    @Test
    public void testDirectExchangeSend() {
        senderService.directExchangeSend();
    }


    //使用Topic型交换机发送消息
    @Test
    public void testTopicExchangeSent() {
        senderService.topicExchangeSend();
    }


    //测试使用fanout型交换机发送消息
    @Test
    public void testFanoutExchange() {
        senderService.fanoutExchangeSend();
    }


    //测试死信队列
    @Test
    public void testDxlExchange() {
        senderService.dxlExchangeSend();
    }


    //测试延迟队列
    @Test
    public void testDelayQueue() {
        senderService.delayQueue();
    }


    //测试消息生产者确认
    @Test
    public void testConfirmMessage() throws InterruptedException {
        senderService.confirmMessage();
        Thread.currentThread().sleep(2000);
    }

}
